const express = require('express');
const path = require('path');
const app = express();

app.use(express.static(path.join(__dirname, 'build')));
app.set('port', (process.env.PORT || 5000));

app.get('/', function (req, res) {
  res.sendFile(path.join(__dirname, 'build', 'index.html'));
});
app.get('/webmail', function(req, res) {
	  res.redirect('http://email.joserodrigo.email/');
	});

app.listen(app.get('port'), function() {
  console.log('JR is running on port', app.get('port'));
});