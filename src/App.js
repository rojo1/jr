import React, { Component } from 'react';
import './App.css';

var titleStyle = {
	fontSize: '11vw',
	color: 'white',
	fontFamily: 'Roboto',
	fontWeight: '100',
	textAlign: 'center'
};

var desStyles =
{
	fontSize: '23px',
	color: 'white',
	fontFamily:'Roboto',
	fontWeight: '100',
	textAlign: 'center'
};

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <h1 style={titleStyle} className="App-title">JR</h1>
        </header>
        <p style={desStyles} className="App-intro">
          Some useless HTML page.
        </p>
		<p style={{textAlign: 'center', fontWeight:'300'}}>
		<a href='mailto:rojo@joserodrigo.email'>Email</a>
		</p>
      </div>
    );
  }
}

export default App;
